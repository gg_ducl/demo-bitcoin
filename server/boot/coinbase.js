var Client = require('coinbase').Client;

module.exports = function(app) { //app is injected by LoopBack
  api = app.get('services');
  app.coinbase = new Client({'apiKey': api.coinbase.key, 'apiSecret': api.coinbase.secret});
};
