var assert = require('chai').should(),
    request = require('supertest'),
    api = request(require('../server/server'));

describe.only('POST /api/address', function() {
  var token = null;

  before(function(done) {
    api
      .post('/api/Users/login')
      .send({ email: 'ducl+6@geenglobal.vn', password: '123123123' })
      .end(function(err, res) {
        token = res.body.id; // Or something
        done();
      });
  });

  it('success', function(done) {
    api
      .post('/api/address?access_token=' + token)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, done);
  });
});
