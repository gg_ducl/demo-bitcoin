var app = require('../../server/server');

module.exports = function(Users) {
  Users.beforeCreate = function(next, data) {
    app.coinbase.createAccount({name: this.email}, function(err, account) {
      data.walletId = account.id;
      next();
    });
  };
};
