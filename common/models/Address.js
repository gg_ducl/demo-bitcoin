var app = require('../../server/server');
var loopback = require('loopback');
var merge = require('../helpers/merge')
module.exports = function(Address) {
  Address.disableRemoteMethod('findOne', true);

  function currentUser() {
    var ctx = loopback.getCurrentContext();
    return ctx && ctx.get('currentUser');
  }

  Address.sendBitcoin = function(Out, Price, description, cb) {
    user = currentUser();

    app.coinbase.getAccount(user.walletId, function(err, account) {
      var hrTime = process.hrtime();
      if(err) return cb(err);
      account.sendMoney(
        {
          'to': Out,
          'amount': Price,
          'description': description,
          'currency': 'BTC',
          'idem': hrTime[0] * 1000000 + hrTime[1] / 1000
        },
        function(err, tx) {
          if(err) return cb(err);
          cb(null, tx);
        }
      );
    });
  }

  Address.remoteMethod(
      'sendBitcoin',
      {
        accepts: [
          {arg: 'out', type: 'string', required: true},
          {arg: 'price', type: 'number', required: true},
          {arg: 'description', type: 'string'}
        ],
        returns: { arg: 'data', type: Address.modelName, root: true },
        http: {path: '/send', verb: 'post'},
        description: 'Send bitcoin'
      }
  );

  Address.create = function(cb) {
    Address.create(null, cb)
  }

  Address.remoteMethod(
      'create',
      {
        accepts: [],
        returns: { arg: 'data', type: Address.modelName, root: true },
        http: {path: '/', verb: 'post'},
        description: 'Create new wallet'
      }
  );

  Address._findById = function(id, cb) {
    Address.findById(id, {}, function(err, data) {
      if(err) return cb(err);
      if(data) return cb(null, data);

      var error = new Error('Object does not exists');
      error.statusCode = 404;
      cb(error);
    });
  }

  Address.remoteMethod(
      '_findById',
      {
        accepts: {arg: 'id', type: 'number', required: true},
        returns: { arg: 'data', type: Address.modelName, root: true },
        http: {path: '/:id', verb: 'get'},
        description: 'Get wallet info with id'
      }
  );

  Address.getList = function(filter, cb) {
    var user = currentUser();
    if(!filter) filter = {};

    filter = merge.extend(filter, {
      "where": {
        "user_id": user.id
      }
    });

    Address.find(filter, function(err, data) {
      if(err) return cb(err);
      cb(null, data);
    });
  }

  Address.remoteMethod(
      'getList',
      {
        accepts: {arg: 'filter', type: 'object'},
        returns: { arg: 'data', type: [Address.modelName], root: true },
        http: {path: '/', verb: 'get'},
        description: 'Get wallets info'
      }
  );

  Address.coinbaseTransactions = function(cb) {
    user = currentUser();

    app.coinbase.getAccount(user.walletId, function(err, account) {
      if (err) return cb(err);
      account.getTransactions(null, function(err, txs) {
        if (err) return cb(err);
        cb(null, txs);
      });
    });
  }

  Address.remoteMethod(
      'coinbaseTransactions',
      {
        accepts: [],
        returns: {arg: 'data', type: 'array', root: true},
        http: {path: '/transactions', verb: 'get'},
        description: 'Get list transactions'
      }
  );


  // Address.observe('access', function addFilter(ctx, next) {
  //     var user = currentUser();
  //     var path = ctx.Model.sharedClass.http.path;
  //     console.log(ctx.Model.sharedClass);
  //     if (path == '/address') {
  //       ctx.query.where = {"user_id": user.id};
  //       next();
  //     } else {
  //       next();
  //     }
  // });

  // Address.beforeRemote('find', function(ctx, remoteMethodOutput, next) {
  //     user = currentUser();
  //
  //     if (ctx.res.req.baseUrl == '/api/address') {
  //       ctx.args.filter = {
  //         "where": {
  //           "user_id": user.id
  //         }
  //       };
  //       next();
  //     } else {
  //       next();
  //     }
  // });

  Address.observe('before save', function updateAddress(ctx, next) {
    user = currentUser();

    if (ctx.isNewInstance) {
      app.coinbase.getAccount(user.walletId, function(err, account) {
        if(err) return next();
        account.createAddress(null, function(err, address) {
          ctx.instance.address = address.address;
          ctx.instance.address_id = address.id;
          ctx.instance.user_id = user.id;
          next();
        });
      });

    } else {
      next();
    }
  });
};
